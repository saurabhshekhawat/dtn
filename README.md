# Ligthning Forecast Application

This project is an application on a typical [AngularJS][angularjs] web app. You can use it
to see on which assest lightning will strike.

We have two data file here lightning.json and asset.json .Using latitude and longitude of lightning.json you can calculate the quadKey. For calculating the quadKey , we have used quadkeytools library. 
Once you get the quadKey it is being comapred with the quadKey mentioned in asset.json. 

Lightning can strike multiple time at a given location but once the asset is shown in the list is not going to re-occur again.


# Complexity 

The complexity for this logic would be O(n^2). As Each element of lightning.json is been compared here with each element of asset.json.
## Getting Started

To get you started you can simply clone the `lightning-forecast` repository and install the dependencies:

### Prerequisites

You need git to clone the `lightning-forecast` repository. You can get git from [here][git].

We also use a number of Node.js tools to initialize and test `lightning-forecast`. You must have Node.js
and its package manager (npm) installed. You can get them from [here][node].

### Clone `lightning-forecast`

Clone the `lightning-forecast` repository using git:

```
git clone 
cd lightning-forecast
```


### Install Dependencies

We have two kinds of dependencies in this project: tools and AngularJS framework code. The tools
help us manage and test the application.

* We get the tools we depend upon and the AngularJS code via `npm`, the [Node package manager][npm].
* In order to run the end-to-end tests, you will also need to have the
  [Java Development Kit (JDK)][jdk] installed on your machine. Check out the section on
  [end-to-end testing](#e2e-testing) for more info.

We have preconfigured `npm` to automatically copy the downloaded AngularJS files to `app/lib` so we
can simply do:

```
npm install
```

Behind the scenes this will also call `npm run copy-libs`, which copies the AngularJS files and
other front end dependencies. After that, you should find out that you have two new directories in
your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/lib` - contains the AngularJS framework files and other front end dependencies

*Note copying the AngularJS files from `node_modules` to `app/lib` makes it easier to serve the
files by a web server.*


For addding quadkeyTool run `npm install quadkeytools`
### Run the Application

We have preconfigured the project with a simple development web server. The simplest way to start
this server is:

```
npm start
```

Now browse to the app at [`http://localhost:8000/index.html#!/alert`][local-app-url].


## Directory Layout

```
app/                  --> all of the source files for the application
  app.css               --> default stylesheet
  core/                 --> all app specific modules
    version/              --> version related components
      version.js                 --> version module declaration and basic "version" value service
      version_test.js            --> "version" value service tests
      version-directive.js       --> custom directive that returns the current app version
      version-directive_test.js  --> version directive tests
      interpolate-filter.js      --> custom interpolation filter
      interpolate-filter_test.js --> interpolate filter tests
  alert/                --> the alert view template and logic
    alert.html            --> the partial template
    alert.js              --> the controller logic
    alert_test.js         --> tests of the controller
  app.js                --> main application module
  index.html            --> app layout file (the main html template file of the app)
  index-async.html      --> just like index.html, but loads js files asynchronously
e2e-tests/            --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
karma.conf.js         --> config file for running unit tests with Karma
package.json          --> Node.js specific metadata, including development tools dependencies
package-lock.json     --> Npm specific metadata, including versions of installed development tools dependencies
```


## Testing
`
There are two kinds of tests in the `lightning-forecast` application: Unit tests and end-to-end tests.

### Running Unit Tests

The `lightning-forecast` app comes preconfigured with unit tests. These are written in [Jasmine][jasmine],
which we run with the [Karma][karma] test runner. We provide a Karma configuration file to run them.

* The configuration is found at `karma.conf.js`.
* The unit tests are found next to the code they are testing and have a `.spec.js` suffix (e.g.
  `alert.spec.js`).

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will start
watching the source and test files for changes and then re-run the tests whenever any of them
changes.
This is the recommended strategy; if your unit tests are being run every time you save a file then
you receive instant feedback on any changes that break the expected code functionality.

You can also ask Karma to do a single run of the tests and then exit. This is useful if you want to
check that a particular version of the code is operating as expected. The project contains a
predefined script to do this:

```
npm run test-single-run
```


<a name="e2e-testing"></a>

### Running End-to-End Tests

The `lightning-forecast` app comes with end-to-end tests, again written in [Jasmine][jasmine]. These tests
are run with the [Protractor][protractor] End-to-End test runner. It uses native events and has
special features for AngularJS applications.

* The configuration is found at `e2e-tests/protractor-conf.js`.
* The end-to-end tests are found in `e2e-tests/scenarios.js`.

Protractor simulates interaction with our web app and verifies that the application responds
correctly. Therefore, our web server needs to be serving up the application, so that Protractor can
interact with it.

**Before starting Protractor, open a separate terminal window and run:**

```
npm start
```

In addition, since Protractor is built upon WebDriver, we need to ensure that it is installed and
up-to-date. The `lightning-forecast` project is configured to do this automatically before running the
end-to-end tests, so you don't need to worry about it. If you want to manually update the WebDriver,
you can run:

```
npm run update-webdriver
```

Once you have ensured that the development web server hosting our application is up and running, you
can run the end-to-end tests using the supplied npm script:

```
npm run protractor
```

This script will execute the end-to-end tests against the application being hosted on the
development server.

**Note:**
Under the hood, Protractor uses the [Selenium Standalone Server][selenium], which in turn requires
the [Java Development Kit (JDK)][jdk] to be installed on your local machine. Check this by running
`java -version` from the command line.

If JDK is not already installed, you can download it [here][jdk-download].


## Updating AngularJS and other dependencies

Since the AngularJS framework library code and tools are acquired through package managers (e.g.
npm) you can use these tools to easily update the dependencies. Simply run the preconfigured script:

```
npm run update-deps
```

This will call `npm update` and `npm run copy-libs`, which in turn will find and install the latest
versions that match the version ranges specified in the `package.json` file.

If you want to update a dependency to a version newer than what the specificed range would permit,
you can change the version range in `package.json` and then run `npm run update-deps` as usual.


## Expected Output

Output in HTML


![](https://bitbucket.org/saurabhshekhawat/dtn/raw/4cabf96c05d56a706ef4523c790fcd37d201da50/output/OutputHtml.JPG)


Output in Console :
![](https://bitbucket.org/saurabhshekhawat/dtn/raw/f6b5ea297d279d3ecbe23eb9927cd4f606cc88ef/output/ConsoleOutput.JPG)

## Contact
 Developer : Saurabh Singh Shekhawat
 Phone No : +31-649920858
 Email Id : saurabh.shekhawat93@gmail.com