/**
 * @name forecastApp
 * @module forecastApp.alert
 * @description
 * This is the forecastApp.alert module. 
 * This contains<br>
 * - {@link 'alert/alert.html' LightningCtrl}
 *
 **/
'use strict';
angular.module('forecastApp.alert', ['ngRoute'])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/alert', {
      templateUrl: 'alert/alert.html',
      controller: 'LightningCtrl'
    });
  }])
/**
 * LightningCtrl
 * @description
 * compares lightning json and asset.json 
 *
 **/
  .controller('LightningCtrl', function ($scope, $http) {
    $http.get('lightning.json').then(function (lightData) {
      $scope.lightnData = lightData.data;
      if (null !== $scope.lightnData) {
        $scope.lightnData.forEach(function (lightItem) {
          let latitude = lightItem.latitude;
          let longtitude = lightItem.latitude;
          /*this is quadkey calculation using quadkeytools lib*/
          let key = exports.locationToQuadkey(latitude, longtitude, 12);
          lightItem.quadKey = key;
        })
        $http.get('assets.json').then(function (assetData) {
          $scope.assetData = assetData.data;
          $scope.finalAssetData = [];
          $scope.assetData.forEach(function (assetItem) {
            let assestStatus = true;
            $scope.lightnData.forEach(function (lightItem) {
              let heartBeat = true;
              /*as required for flashType ==9: it skips*/
              if (lightItem.flashType === 9) {
                heartBeat = false;
              }
              /*condition to compare lightning.json and asset.json */
              if (heartBeat && assestStatus && lightItem.quadKey === assetItem.quadKey) {
                $scope.finalAssetData.push(assetItem);
                assestStatus = false;
                console.log("lightning alert for " + assetItem.assetOwner + ":" + assetItem.assetName);
              }
            })
          });

        },
          function (error) {
            console.error("Invalid Asset Data");
          });
      }

    },
      function (error) {
        console.error("Invalid Lightning Data");
      });


  });