/**
 * @description
 * test case for alert.js and LightningController
 * @param {function} angular Application Dependency
 */
'use strict';

describe('forecastApp.alert module', function () {

  beforeEach(module('forecastApp.alert'));
  var $controller, $rootScope, authRequestHandler, $httpBackend, $window, assetRequestHandler;

  beforeEach(inject(function ($injector, _$controller_, _$rootScope_, _$window_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $httpBackend = $injector.get('$httpBackend');
    authRequestHandler = $httpBackend.when('GET', 'lightning.json')
      .respond([{}]);
    assetRequestHandler = $httpBackend.when('GET', 'assets.json').respond(
      [{}]);
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $window = _$window_;
  }));

  describe('alert controller', function () {
    it('should check for the right controller', inject(function ($controller) {
      var $scope = $rootScope.$new();
      var alertCtrl = $controller('LightningCtrl', { $scope: $scope });
      expect(alertCtrl).toBeDefined();
    }));

    it('should read valid json for lightning data and assest', function () {
      authRequestHandler.respond(200, [{
        "flashType": 1,
        "strikeTime": 1446761133619,
        "latitude": 33.032458,
        "longitude": -98.3053053,
        "peakAmps": 7500,
        "reserved": "000",
        "icHeight": 15529,
        "receivedTime": 1446761145726,
        "numberOfSensors": 19,
        "multiplicity": 6
      },
      {
        "flashType": 1,
        "strikeTime": 1446761131759,
        "latitude": 6.356331,
        "longitude": -14.9094801,
        "peakAmps": -6127,
        "reserved": "000",
        "icHeight": 19813,
        "receivedTime": 1446761145726,
        "numberOfSensors": 7,
        "multiplicity": 1
      }]);

      $httpBackend.expectGET('lightning.json');
      assetRequestHandler.respond(200,
        [{
          "assetName": "Mayer Park",
          "quadKey": "023112133002",
          "assetOwner": "02115"
        }]);
      $httpBackend.expectGET('assets.json');
      var $scope = $rootScope.$new();
      $window.exports = { locationToQuadkey: function () { return parseInt('133002') } };
      var alertCtrl = $controller('LightningCtrl', { $scope: $scope });
      $httpBackend.flush();
      expect($scope.lightnData).toEqual([{
        "flashType": 1,
        "strikeTime": 1446761133619,
        "latitude": 33.032458,
        "longitude": -98.3053053,
        "peakAmps": 7500,
        "reserved": "000",
        "icHeight": 15529,
        "receivedTime": 1446761145726,
        "numberOfSensors": 19,
        "multiplicity": 6,
        "quadKey": 133002
      },
      {
        "flashType": 1,
        "strikeTime": 1446761131759,
        "latitude": 6.356331,
        "longitude": -14.9094801,
        "peakAmps": -6127,
        "reserved": "000",
        "icHeight": 19813,
        "receivedTime": 1446761145726,
        "numberOfSensors": 7,
        "multiplicity": 1,
        "quadKey": 133002
      }])
      expect($scope.assetData).toEqual(
        [{
          "assetName": "Mayer Park",
          "quadKey": "023112133002",
          "assetOwner": "02115"
        }])
    });

  });

});