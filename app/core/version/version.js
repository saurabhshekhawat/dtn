'use strict';

angular.module('forecastApp.version', [
  'forecastApp.version.interpolate-filter',
  'forecastApp.version.version-directive'
])

.value('version', '0.1');
